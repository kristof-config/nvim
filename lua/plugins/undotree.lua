
local M = {
    'mbbill/undotree',
    cmd = {"UndotreeToggle"},
    lazy = false,
    config = function ()
        vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
    end
}

return { M }
