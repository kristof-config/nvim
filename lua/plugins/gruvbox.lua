
return {
    "ellisonleao/gruvbox.nvim",
    name = "gruvbox",
    --lazy = true,
    priority = 1000,
    config = function()
        -- Using protected call
        local status_ok, gruvbox = pcall(require, "gruvbox")
        if not status_ok then
            return
        end
        vim.cmd.colorscheme("gruvbox")
    end
}
