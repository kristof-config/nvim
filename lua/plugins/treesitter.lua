--
return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function ()
        local configs = require("nvim-treesitter.configs")

        configs.setup({
            ensure_installed = {
                "c", "lua", "vim", "vimdoc", "query",
                "typescript", "go", "gopls", "rust_analyzer", "python",
                "javascript"
            },
            sync_install = false,
            auto_install = true,
            highlight = { enable = true },
            indent = { enable = true }, })
    end,

    {'nvim-treesitter/playground'}

}
