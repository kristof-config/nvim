
local M = {
    'tpope/vim-fugitive',
    cmd = {"Git"},
    lazy = false,
    config = function ()
        vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
    end
}

return { M }
