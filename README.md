## My dotfile neovim with lazy nvim and theprimeagen config

```
./
├── after/
│   └── plugins/
├── init.lua
├── lazy-lock.json
├── lua/
│   ├── core/
│   │   ├── options.lua
│   │   └── set.lua
│   ├── lazy-setup.lua
│   └── plugins/
│       ├── fugitive.lua
│       ├── gruvbox.lua
│       ├── harpoon.lua
│       ├── lsp.lua
│       ├── lualine.lua
│       ├── telescope.lua
│       ├── treesitter.lua
│       └── undotree.lua
└── README.md
```
----------------
### CREDIT

> [ThePrimeagen](https://github.com/ThePrimeagen/init.lua)<br>
> [sadikeey](https://github.com/sadikeey/nvim)<br>
> [Traap](https://github.com/Traap/nvim)<br>

